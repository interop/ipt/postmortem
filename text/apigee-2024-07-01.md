# Apigee API Gateway Outage

Date: 2024-07-01

## Summary

Starting on July 1st, 2024, all APIs proxied through Apigee in all organizations and environments started returning HTTP 502 "Bad Gateway" errors.
The Developer Portal, Apigee Management APIs, and the Apigee Web UI were not affected by this outage.

The component of Apigee that failed is part of the infrastructure that is maintained by the API Team.
The components of Apigee managed by Google was not the cause of the outage.

Setting up Apigee X in Google Cloud Platform (GCP) involves creating a [Managed Instance Group (MIG)](https://cloud.google.com/apigee/docs/api-platform/get-started/install-cli#external-routing-mig) that forwards traffic from an external load balancer to the internal Apigee instances.
The MIG is a group of Virtual Machines (VM) that are configured identically and traffic is routed to them from a load balancer.
The MIG inherits its settings from an Instance Template so that VMs start the same way every time.
The MIG ensures that an expected number of VMs are running, and increases the count of VMs running if their usage exceeds a certain threshold.

The VMs in the MIG are [preemptible](https://cloud.google.com/compute/docs/instances/preemptible), meaning that they sometimes are taken offline and replaced with a new VM automatically by Google, mainly as a cost saving measure.
The instance template for the MIG was configured to start the VMs using Debian 10, which reached end-of-life on June 30th 2024, and was no longer able to be used by new VMs in GCP after that date.

As VMs in the MIG were being preempted and therefore terminated, new VMs starting up after June 30th failed to start because Debian 10 was no longer a valid OS option.
The count of running VMs in the MIG slowly dropped as VMs were being preempted and new ones failed to start.
The last production VM was terminated around 11:03 PM Central Time on July 1st, 2024, causing a full outage of all Apigee API Gateway traffic.

After determining the cause of the outage, the API Team resolved the outage by updating the instance template to use Debian 12.
This caused VMs to start and function normally, allowing traffic to Apigee to resume as expected.

## Impact

All APIs were unreachable through Apigee during the outage, including the Person API, HR API, Finance API, DARS API, and Google Groups API.
This caused outages for downstream applications that use these APIs, such as the Course Search & Enroll application.

## Timeline

All times are approximate and in the Central Timezone.

**July 1st 2024, 9:00 AM:** Debian 10 OS Image became unavailable to use for new VMs. VMs start going offline as they are preempted and cannot start because Debian 10 is unavailable.

**July 1st, 11:03 PM:** The last VM in the MIG was preempted and therefore terminated. Apigee API traffic outage begins.

**July 2nd, 11:00 AM:** API Team updated the Instance Template to use Debian 12. New VMs come online and Apigee API traffic is restored.

## Root Cause(s)

The combination of Debian 10 becoming unavailable for new VMs, along with the VMs being terminated as part as expected preemption activities, reduced the number of running VMs in the MIG to zero.
This blocked all API gateway traffic to Apigee until new VMs came online.

## Action Items

Actions should be taken that would help prevent an outage like this in the future:

- **Alert on the number of VMs in the MIG:** The minimum number of VMs in the MIG is 3. If the count drops below this number, the API Team should be alerted.
- **Notifications for OS deprecations:** The API Team should be notified for upcoming deprecations of operating systems that are in use.

These actions help with the existing MIG-based infrastructure for Apigee.
However, there now exists a newer method for [routing traffic to Apigee using a Private Service Connect (PSC)](https://cloud.google.com/apigee/docs/api-platform/get-started/install-cli#external-routing-psc), which doesn't require maintaining a MIG.
Work should be done to migrate this core component of Apigee from the original MIG-based setup to the new PSC configuration, which would greatly reduce maintenance overhead for this infrastructure.

## Lesson Learned

Maintaining VMs continues to be a hotspot for maintenance overhead. Extra care should be taken to monitor VMs and make sure they are running the latest software.
When possible, prefer managed services or serverless offerings over maintaining VMs.

## References

- [DoIT Outage](https://outages.doit.wisc.edu/outage/94acdbe5f1d1925aed2f654d3c8a65e2bb22036b93)
