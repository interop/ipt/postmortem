# ActiveMQ Outage

Date: 2021-11-17

## Summary

At 7:33 AM on November 17th, 2021, the Integration Platform Team became aware of an issue with the Course Search & Enroll application having issues publishing messages to ActiveMQ.
Upon investigating, the issue was confirmed to be caused by the production ActiveMQ broker reaching a configured quota for the amount of space occupied by persistent messages (10 gigabytes).
ActiveMQ was configured to not accept any new messages until existing messages were consumed or otherwise removed.

## Impact

At any time of year, but especially during an enrollment period, the biggest impact of this outage was on the Course Search & Enroll application.
Students couldn't determine whether they were eligible to enroll in a class ahead of their scheduled enrollment time, and they couldn't enroll in any classes.
Additionally, real-time updates from CAOS to Course Search & Enroll weren't able to go through, since CAOS was unable to publish messages to ActiveMQ.

Other services also depend on ActiveMQ and were impacted by this outage.
Services such as Office365 depend on ActiveMQ to be notified when NetIDs are disabled and should be deactivated.
PICH also depends on ActiveMQ to integrate identity data with other UW System campuses.

## Timeline

ActiveMQ logs did not go far back enough to identify the exact time when the broker stopped accepting messages.
Course Search & Enroll started noticing issues with publishing messages at 12:15 AM on 11/17.
After the Integration Platform Team became aware of the issue and was able to investigate, certain queues were able to manually purged to delete messages and free up space for new messages to be published at 8:42 AM.

## Root Cause(s)

Normally, messages on ActiveMQ are consumed within a day, so messages older than that usually indicate an unused queue or a problem with the consuming system.

Leading up to the time of the outage, message consumers and publishers have been migrating to a new ActiveMQ broker in AWS (AmazonMQ). 
One consuming system, using a [durable subscription](https://activemq.apache.org/virtual-destinations), had migrated to AmazonMQ in late October 2021 but messages had continued to accumulate on the queue in ActiveMQ.
All except one consuming system use virtual topics instead of durable subscriptions, so when those systems migrate to AmazonMQ, messages are directed to their new queue instead of continuing to accumulate on ActiveMQ.

The amount of messages persisting on the ActiveMQ broker slowly increased since the durable subscription consumer was migrated to AmazonMQ.
High message activity during enrollment was enough to consume the remainder of the persistent message quota, leading to the ActiveMQ broker refusing to accept new messages from all publishers.

## Action Items

Although the outage was a related to migrating off of ActiveMQ, it's important to note that this issue was a result of ActiveMQ behaving as expected, and these issues could occur in AmazonMQ.
These are some action items that could help prevent this issue in the future.

- **Message TTL (Time To Live)**: Currently, messages persist indefinitely unless they are consumed or otherwise deleted. As a common practice in event-driven architectures, applying a TTL to messages would allow messages to be deleted automatically if they are not consumed within a certain timeframe. Jira ticket [INPLATFORM-859](https://jira.doit.wisc.edu/jira/browse/INPLATFORM-859)
- **Monitor and Alert on Storage Usage**: Even with a TTL for messages, it is still possible for the storage quota on ActiveMQ to be consumed. Monitoring and alerts should be setup so that the Integration Platform Team can take action if a considerable amount of the storage quota is occupied. Jira ticket [INPLATFORM-860
](https://jira.doit.wisc.edu/jira/browse/INPLATFORM-860)
- **Clean Up of Unused Configurations**: There are some consumer queues that are no longer needed, so those queues should be removed. Jira ticket [INPLATFORM-844](https://jira.doit.wisc.edu/jira/browse/INPLATFORM-844)

## Lesson Learned

* Understand the quotas and limits of ActiveMQ, document them for users, and alert the Integration Platform Team if their usage could cause a disruption for other users.
* Avoid durable subscriptions in the future. ActiveMQ has some [documentation with some of their limitations](https://activemq.apache.org/virtual-destinations).

## References

- [ActiveMQ Outage (DoIT)](https://outages.doit.wisc.edu/outage/947b887bd21367f97c9ffa46b89a29ee8eb0bf6d1e)
- [Microsoft Teams thread troubleshooting the ActiveMQ outage](https://teams.microsoft.com/l/message/19:06f6d61c5ebf4cfcac6d0db9c92f2972@thread.skype/1637156022115?tenantId=2ca68321-0eda-4908-88b2-424a8cb4b0f9&groupId=d37cda49-af2d-40c6-93d5-c3a2923f14c6&parentMessageId=1637156022115&teamName=AIS-Interop-Enterprise%20Integration&channelName=IP%20team&createdTime=1637156022115)
- [Course Search & Enroll Outage Notes](https://docs.google.com/document/d/1mT-Hz5707wM-YP1ZUEQs1pjcy3NfJZQLQQJX4kj1ScA/edit#heading=h.oz37yz52i9hc)
- [Course Search & Enroll Post-Incident Report](https://docs.google.com/document/d/10bt_JohGs_abakykUJZDI54SVunK4-ZXQ384cg5IrUM/edit)
