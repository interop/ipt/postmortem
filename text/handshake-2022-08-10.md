> Template from the [Real-world SRE](http://a.co/d/34UsUVU) book.

# Handshake Outage

Date: 2022-08-10

## Summary

Handshake imports had not been running since the end of June, but neither us nor the stakeholders had noticed.

## Impact

Handshake user data had fallen out of sync.  Any data that changed in July hadn't been updated in Handshake.

## Timeline

* On 2021-10-11 our Handshake Teams connection expired, and we stopped receiving alerts on the status of Handshake imports.
* At the end of June, an old version of the Handshake importer was running in AWS that was failing everytime 
the importer was run.
* In early August we switched Handshake over to Gitlab CI/CD, but a bug in the Dockerfile prevented in the importer 
from running.
* After we became aware of this issue on 2022-08-10, we found and fixed the bug in the Dockerfile and the correct version 
of the importer was deployed.  We then manually ran it to get the data back in sync.

## Root Cause(s)

* Jenkins pipeline wasn't deploying the correct version of our application.
* GitLab pipeline was deploying a version of our application that couldn't start up.
* Teams alert channel was expired so we weren't getting notifications about failed runs. 

## Action Items

* Fix the alert channel
  * Clarify if we should re-run import manually after a failure occurs, or wait until next automatic run.
* Run the importer in test when we merge MRs, ideally automatically
  * Even better if the change isn't deployed to production if the run in test fails.  This will require a most robust 
  deployment process.

## Lesson Learned

* Alert channels shouldn't be hidden

## References
