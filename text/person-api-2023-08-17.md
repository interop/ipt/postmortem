# Person API Outage

Date: 2023-08-17

## Summary

At 9:30 PM on August 17th, 2023, the API Team became aware of increased error rates with the Person API through alerts sent to Salesforce Service Cloud.
The issue was fixed and Person API was returned to normal service by 9:39 PM.

## Impact

Errors started occurring at 3:45 PM and stopped when the issue was resolved at 9:39 PM.
There were around 3000 total requests to the Person API, and around 411 of those requests resulted in a 500 Internal Server Error.

## Root Cause(s)

The issue was caused by an incomplete deployment of the Person API.
A change was being deployed to add a new field to `jobs` called `position`, which required a new column to be added in the Person API database.

Production deployments occur after a change is merged into the main branch of the Person API GitLab repository, but the actual deployment itself is triggered manually by a team member instead of being triggered automatically by the merge into the main branch.
Changes to the code occur before changes to the database, but these two steps both required a manual trigger, instead of database changes being automatically triggered upon a successful deployment of the code changes.

The code was deployed, but the subsequent database changes were never triggered by a team member.
As a result, some requests were failing because they were looking for the new `position` column, but it was never added to the database.
The issue was fixed at 9:39 PM when the new column was added to the database.

## Action Items

* **Deploy database changes before code changes:** Modify the deployment pipeline to deploy database changes first, and then automatically trigger code changes after database changes are successful.
* **Alerting improvements**: Alerts were sent to Service Cloud which creates new cases, but new cases do not trigger a notification to the sprint's support person. Have a way to notify a support person of new cases, and have a way to indicate priority/urgency so that human-initiated cases get attention faster.

## References

- [Person API Outage (DoIT)](https://outages.doit.wisc.edu/outage/949c68606d70ef6aec24ec4ac38acb71fe4faf38b8)
- WiscIT Change Record: 111040