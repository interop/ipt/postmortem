# Integration Platform Team Postmortems

Collections of [postmortems](https://www.atlassian.com/incident-management/handbook/postmortems) for outages of services provided by the Integration Platform team.

Our goal is to have [blameless](https://codeascraft.com/2012/05/22/blameless-postmortems/) postmortems.

Example postmortems: https://github.com/danluu/post-mortems
