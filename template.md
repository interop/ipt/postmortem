> Template from the [Real-world SRE](http://a.co/d/34UsUVU) book.

# Title of the incident

Date:

## Summary

## Impact

## Timeline

## Root Cause(s)

## Action Items

## Lesson Learned

## References
